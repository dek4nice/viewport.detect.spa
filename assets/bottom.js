window.shouldRound = true;
jQuery("input[type=checkbox][data-action]").each(function() {
  function toggle() {
    var action = new Function(this.getAttribute("data-action"));
    action.call(this);
  }
  function onclick() {
    toggle.call(this);
    recalcValues();
  }
  this.onclick = onclick;
  toggle.call(this);
});

function showFontsize() {
  var link = document.getElementById('testlink__value');
  var style = window.getComputedStyle(link , null);
  var html = document.childNodes[1];
  document.getElementById('testlink__one').innerText = window.getComputedStyle(html).fontSize;
  document.getElementById('testlink__two').innerText = style.getPropertyValue('font-size');
  document.getElementById('testlink__three').innerText = style.fontSize;
}

function recalcValues() {
  var list = [];
  function evalJS() {
    var expr = jQuery(this).find(".code").text();
    if (! /\S/.test(expr)) return;
    if (-1 === expr.indexOf(";")) {
      expr = "return (" + expr + ");";
    }
    var value = new Function(expr)();
    if (window.shouldRound && "number" === typeof value)
      value = Math.round(value*100)/100;
    list.push({cell: jQuery(this).find(".value"), value: value});
  }
  jQuery("#units tr").each(evalJS);
  jQuery("#units .rowdata").each(evalJS);
  jQuery(".eval").each(evalJS);
  jQuery.each(list, function() {
    this.cell.text(this.value);
  });

  showFontsize();

}
// recalcValues();
$('#calc').click(recalcValues);
// $(window).resize(recalcValues);
// $(window).click(recalcValues);
